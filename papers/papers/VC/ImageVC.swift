//
//  ImageVC.swift
//  papers
//
//  Created by Александр on 29/03/2019.
//  Copyright © 2019 Александр Андрюшин. All rights reserved.
//

import UIKit

class ImageVC: UIViewController {

    @IBOutlet weak var imageview: UIImageView!
    
    var imageName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        guard let name = imageName else { return }
        if let image = UIImage(named: name) {
            imageview.image = image
        }
    }
}
