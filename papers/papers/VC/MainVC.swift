//
//  MainVC.swift
//  papers
//
//  Created by Александр on 29/03/2019.
//  Copyright © 2019 Александр Андрюшин. All rights reserved.
//

import UIKit

struct Item {
    var imageName: String
}

class MainVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var items: [Item] = [Item(imageName: "1"),
                         Item(imageName: "2"),
                         Item(imageName: "3"),
                         Item(imageName: "4"),
                         Item(imageName: "5"),
                         Item(imageName: "6"),
                         Item(imageName: "7"),
                         Item(imageName: "8"),
                         Item(imageName: "9"),
                         Item(imageName: "10"),
                         Item(imageName: "11"),
                         Item(imageName: "12"),
                         Item(imageName: "13"),
                         Item(imageName: "14"),
                         Item(imageName: "15"),
                         Item(imageName: "16"),
                         Item(imageName: "17"),
                         Item(imageName: "18")]
    
    let numberOfItemPerRow: CGFloat = 2
    let lineSpacing: CGFloat = 5
    let interItemSpacing: CGFloat = 5
    
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupCollectionViewItemSize()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let item = sender as! Item
        
        if segue.identifier == "Segue" {
            if let vc = segue.destination as? ImageVC {
                vc.imageName = item.imageName
            }
        }
    }
    
    private func setup() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "Cell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    private func setupCollectionViewItemSize() {
        let customLayout = CustomLayout()
        customLayout.delegate = self
        collectionView.collectionViewLayout = customLayout
        
    }
}

extension MainVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Cell
        
        cell.imageView.image = UIImage(named: items[indexPath.item].imageName)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        performSegue(withIdentifier: "Segue", sender: item)
    }
    
    
}
extension MainVC: CustomLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, sizeOfPhotoAtIndexPath indexPath: IndexPath) -> CGSize {
        return UIImage(named: items[indexPath.item].imageName)!.size
    }
}


