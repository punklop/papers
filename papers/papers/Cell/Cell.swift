//
//  Cell.swift
//  papers
//
//  Created by Александр on 29/03/2019.
//  Copyright © 2019 Александр Андрюшин. All rights reserved.
//

import UIKit

class Cell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
    }
}
